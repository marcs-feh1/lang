.data
__runtime.input_int: .string "(int) << "
__runtime.input_str: .string "(string) << "
__runtime.output: .string ">> "
__runtime.newline: .string "\n"

.align 2
square__result: .space 4
.align 2
circleArea__result: .space 4
.align 2
PI_i_fb414474: .space 4
.align 2
r2_i_968bda27: .space 4
.align 2
r_P_i_4f6371af: .space 4
.align 2
r_i_d68c3ee7: .space 4
.align 2
x_P_i_fd0c5087: .space 4

.text
j __runtime.entry_point

# Write a single newline char to the console
__runtime.write_newline:
	addi sp, sp, -4 # Push ra to stack
	sw ra, (sp)

	li a7, 4
	la a0, __runtime.newline
	ecall

	lw ra, (sp) # Restore return address
	addi sp, sp, 4
	ret

# Write a null terminated string to console
__runtime.write_string:
	lw a0, (sp) # Pop argument into a0
	addi sp, sp, 4
	addi sp, sp, -4 # Push ra to stack
	sw ra, (sp)

	li a7, 4
	ecall

	lw ra, (sp) # Restore return address
	addi sp, sp, 4
	ret

# Write integer to console.
__runtime.write_int:
	lw a0, (sp) # Pop integer into a0 (arg for syscall)
	sw ra, (sp) # Push return address, for later usage

	li a7, 1  # Set system service ID
	ecall     # Perform the syscall

	lw ra, (sp)    # Restore return address by popping it off the stack
	addi sp, sp, 4
	ret

# Read integer from console, push result to stack
__runtime.read_int:
	addi sp, sp, -4 # Push return value to stack
	sw zero, (sp)
	addi sp, sp, -4 # Push ra to stack
	sw ra, (sp)

	addi sp, sp, -4             # Push input prompt
	la t0, __runtime.input_int
	sw t0, (sp)
	call __runtime.write_string # Print input prompt

	li a7, 5 # Issue readInt syscall
	ecall

	lw ra, (sp)    # Restore return address by popping it off the stack
	addi sp, sp, 4
	sw a0, (sp) # Set return value
	ret

# Program entry point
__runtime.entry_point:
	call main
	j __runtime.exit

	# Expand program here
		square:
	
	# Store_To 
	lw s0, (sp)
	la s1, x_P_i_fd0c5087
	sw s0, (s1)
	addi sp, sp, 4
	
	# Push return address
	addi sp, sp, -4
	sw ra, (sp)
	
	# Push x_P_i_fd0c5087
	la s0, x_P_i_fd0c5087
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Push x_P_i_fd0c5087
	la s0, x_P_i_fd0c5087
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Mul 
	lw s0, (sp)
	lw s1, 4(sp)
	mul s0, s1, s0
	addi sp, sp, 4
	sw s0, (sp)
	
	# Store_To 
	lw s0, (sp)
	la s1, square__result
	sw s0, (s1)
	addi sp, sp, 4
	j square__exit
	square__exit:
	
	# Restore return address
	lw ra, (sp)
	addi sp, sp, 4
	
	# Load_From 
	lw s0, square__result
	addi sp, sp, -4
	sw s0, (sp)
	
	ret
	circleArea:
	
	# Store_To 
	lw s0, (sp)
	la s1, r_P_i_4f6371af
	sw s0, (s1)
	addi sp, sp, 4
	
	# Push return address
	addi sp, sp, -4
	sw ra, (sp)
	
	# Push PI_i_fb414474
	la s0, PI_i_fb414474
	addi sp, sp, -4
	sw s0, (sp)
	
	# Push 314
	li s0, 314
	addi sp, sp, -4
	sw s0, (sp)
	
	# Store 
	lw s0, (sp)
	lw s1, 4(sp)
	sw s0, (s1)
	addi sp, sp, 8
	
	# Push r2_i_968bda27
	la s0, r2_i_968bda27
	addi sp, sp, -4
	sw s0, (sp)
	
	# Push r_P_i_4f6371af
	la s0, r_P_i_4f6371af
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Call square
	call square
	
	# Push 100
	li s0, 100
	addi sp, sp, -4
	sw s0, (sp)
	
	# Mul 
	lw s0, (sp)
	lw s1, 4(sp)
	mul s0, s1, s0
	addi sp, sp, 4
	sw s0, (sp)
	
	# Store 
	lw s0, (sp)
	lw s1, 4(sp)
	sw s0, (s1)
	addi sp, sp, 8
	
	# Push r2_i_968bda27
	la s0, r2_i_968bda27
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Call_Builtin __write_int
	call __runtime.write_int
	
	# Call_Builtin __write_newline
	call __runtime.write_newline
	
	# Push r2_i_968bda27
	la s0, r2_i_968bda27
	addi sp, sp, -4
	sw s0, (sp)
	
	# Push PI_i_fb414474
	la s0, PI_i_fb414474
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Push r2_i_968bda27
	la s0, r2_i_968bda27
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Mul 
	lw s0, (sp)
	lw s1, 4(sp)
	mul s0, s1, s0
	addi sp, sp, 4
	sw s0, (sp)
	
	# Store 
	lw s0, (sp)
	lw s1, 4(sp)
	sw s0, (s1)
	addi sp, sp, 8
	
	# Push r2_i_968bda27
	la s0, r2_i_968bda27
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Push 100
	li s0, 100
	addi sp, sp, -4
	sw s0, (sp)
	
	# Div 
	lw s0, (sp)
	lw s1, 4(sp)
	div s0, s1, s0
	addi sp, sp, 4
	sw s0, (sp)
	
	# Store_To 
	lw s0, (sp)
	la s1, circleArea__result
	sw s0, (s1)
	addi sp, sp, 4
	j circleArea__exit
	circleArea__exit:
	
	# Restore return address
	lw ra, (sp)
	addi sp, sp, 4
	
	# Load_From 
	lw s0, circleArea__result
	addi sp, sp, -4
	sw s0, (sp)
	
	ret
	main:
	
	# Push return address
	addi sp, sp, -4
	sw ra, (sp)
	
	# Push r_i_d68c3ee7
	la s0, r_i_d68c3ee7
	addi sp, sp, -4
	sw s0, (sp)
	
	# Call_Builtin __read_int
	call __runtime.read_int
	
	# Store 
	lw s0, (sp)
	lw s1, 4(sp)
	sw s0, (s1)
	addi sp, sp, 8
	
	# Push r_i_d68c3ee7
	la s0, r_i_d68c3ee7
	addi sp, sp, -4
	sw s0, (sp)
	
	# Load
	lw s0, (sp)
	lw s1, (s0)
	sw s1, (sp)
	
	# Call circleArea
	call circleArea
	
	# Call_Builtin __write_int
	call __runtime.write_int
	main__exit:
	
	# Restore return address
	lw ra, (sp)
	addi sp, sp, 4
	
	ret
	

__runtime.exit:


